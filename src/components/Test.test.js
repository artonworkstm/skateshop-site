const sum = require("./Test");

test("test sum func", () => {
  const goal = sum(5, 5);
  expect(goal).toBe(10);
});
